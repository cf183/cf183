#include<stdio.h>
void input(int *a,int *b)
{
	printf("Enter two integers:\n");
	scanf("%d%d",a,b);
}
void output1(int *a,int *b)
{
	printf("Before swapping:First number=%d and second number=%d\n",*a,*b);
}
void swap(int *a,int *b)
{
	int temp;
	temp=*a;
	*a=*b;
	*b=temp;
}
void output2(int *a,int *b)
{
	printf("After swapping:First number=%d and second number=%d\n",*a,*b);
}
int main()
{
	int a,b;
	input(&a,&b);
	output1(&a,&b);
	swap(&a,&b);
	output2(&a,&b);
	return 0;
}

