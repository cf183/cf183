#include<stdio.h>
void input(float *principal,float *rate_of_interest,int *time_in_years)
{
    printf("Enter the principal amount,rate of interest and time in years\n");
    scanf("%f%f%d",principal,rate_of_interest,time_in_years);
}
void compute(float principal,float rate_of_interest,int time_in_years,float *simple_interest)
{
	 *simple_interest=(principal*rate_of_interest*time_in_years)/100;
}
void output(float principal,float rate_of_interest,int time_in_years,float simple_interest)
{
	printf("The simple interest for the amount %f at the rate of interest %f and time %d is:%.2f\n",principal,time_in_years,rate_of_interest,simple_interest);
}
int main()
{
	float pri,rate,sim;
	int time;
	input(&pri,&rate,&time);
	compute(pri,rate,time,&sim);
	output(pri,rate,time,sim);
	return 0;
}