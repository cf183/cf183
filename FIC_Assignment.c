#include<stdio.h>
#include<math.h>
float input()
{
    float a;
    printf("Enter the length of a side\n");
    scanf("%f",&a);
    return a;
}
float compute(float a,float b,float c)
{
    float semi_perimeter,area_of_triangle;
    semi_perimeter=(a+b+c)/2;
    area_of_triangle=sqrt((semi_perimeter)*(semi_perimeter-a)*(semi_perimeter-b)*(semi_perimeter-c));
    return area_of_triangle;
}
void output(float a,float b,float c,float area_of_triangle)
{
    printf("The area of triangle having side lengths:%f,%f and %f is %f",a,b,c,area_of_triangle);
}
float main()
{
    float x,y,z,area_of_triangle;
    x=input();
    y=input();
    z=input();
    area_of_triangle=compute(x,y,z);
    output(x,y,z,area_of_triangle);
    return 0.0;
}

