#include<stdio.h>
struct student
{
	int rollno;
	char name[25];
	char section;
	char department[3];
	float fees;
	int marks;
};
typedef struct student Student;
Student input(int num)
{
	Student s;
	printf("Enter the roll no,name,section,department,fees and the total marks of student%d:\n",num);
	scanf("%d",&s.rollno);
	getchar();
	gets(s.name);
	scanf("%c",&s.section);
	getchar();
	gets(s.department);
	scanf("%f%d",&s.fees,&s.marks);
	return s;
}
Student compute(Student s1,Student s2)
{
	if(s1.marks>s2.marks)
		return s1;
	else
		return s2;
}
void output(Student s)
{
	printf("The details of the student who has scored the highest marks:\nRoll no:%d\nName:%s\nSection:%c\nDepartment:%s\nFees:%f\nMarks:%d\n",s.rollno,s.name,s.section,s.department,s.fees,s.marks);
}
int main()
{
	Student s1,s2,s;
	int no=1;
	s1=input(no);
	int n=2;
	s2=input(n);
	s=compute(s1,s2);
	output(s);
	return 0;
}