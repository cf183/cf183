#include<stdio.h>
struct complex
{
	float real;
	float imaginary;
};
typedef struct complex Complex;
void input(int *a,int *b,int *c)
{
	printf("Enter the three coefficients:\n");
	scanf("%d%d%d",a,b,c);
}
void compute(int a,int b,int c,Complex *r1,Complex *r2)
{
	int d;
	d=(b*b)-(4*a*c);
	if(d==0)
	{
		printf("The roots are real and equal\n");
		(*r1).real=-b/(2*a);
		(*r1).imaginary=0;
		(*r2).real=-b/(2*a);
		(*r2).imaginary=0;
	}
	else if(d>0)
	{
		printf("The roots are real and distinct\n");
		r1->real=(-b+sqrt(d))/(2*a);
		r1->imaginary=0;
		r2->real=(-b-sqrt(d))/(2*a);
		r2->imaginary=0;
	}
	else
	{
		printf("The roots are imaginary\n");
		(*r1).real=0;
		(*r1).imaginary=(-b+sqrt(fabs(d)))/(2*a);
		(*r2).real=0;
		(*r2).imaginary=(-b-sqrt(fabs(d)))/(2*a);
	}
}

void output(Complex r1,Complex r2)
{

	printf("The roots are %f,%f,%f and %f\n",r1.real,r1.imaginary,r2.real,r2.imaginary);
}
int main()
{
	int a,b,c;
	Complex r1,r2;
	input(&a,&b,&c);
	compute(a,b,c,&r1,&r2);
	output(r1,r2);
	return 0;
}







