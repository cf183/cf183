#include<stdio.h>
struct book
{
	char title[25];
	char author[25];
	float price;
	int pages;
};
typedef struct book Book;
void input(Book *b1,Book *b2)
{
	printf("Enter the title,author ,price and total number of pages of book1:\n");
	scanf("%s%s%f%d",b1->title,b1->author,&(b1->price),&(b1->pages));
	printf("Enter the title,author,price and total number of pages of book2:\n");
	scanf("%s%s%f%d",b2->title,b2->author,&(b2->price),&(b2->pages));
}
void output1(Book b1,Book b2)
{
if(b1.price>b2.price)
	printf("The title of the book which is expensive is %s\n",b1.title);
else
	printf("The title of the book which is expensive is %s\n",b2.title);
}
void output2(Book b1,Book b2)
{
if(b1.pages<b2.pages)
	printf("The author of the book which has less number of pages is %s\n",b1.author);
else
	printf("The author of the book which has less number of pages  is %s\n",b2.author);
}
int main()
{
Book b1;
Book b2;
input(&b1,&b2);
output1(b1,b2);
output2(b1,b2);
return 0;
}