#include<stdio.h>
int input()
{
	int x;
	printf("Enter a number:\n");
	scanf("%d",&x);
	return x;
}
int compute(int a,int b,int c)
{
	if(a>=b&&a>=c)
	{
		return a;
	}
	else if(b>=c)
	{
		return b;
	}
	else
	{
		return c;
	}
}
void output(int a,int b,int c,int r)
{
    printf("The greatest among the three integers %d ,%d and %d is %d\n",a,b,c,r);
}
int main()
{
	int a,b,c;
	a=input();
	b=input();
	c=input();
	int r;
	r=compute(a,b,c);
	output(a,b,c,r);
	return 0;
}
