#include<stdio.h>
void input(FILE *fp)
{
    char ch;
    fp=fopen("INPUT.txt","w");
    printf("Enter the file contents:\n");
    while((ch=getchar())!=EOF)
    {
        putc(ch,fp);
    }
    fclose(fp);
}
void output(FILE *fp)
{
    char ch;
    fp=fopen("INPUT.txt","r");
    printf("The file contents are:");
    while((ch=getc(fp))!=EOF)
    {
        printf("%c",ch);
    }
    fclose(fp);
}
int main()
{
    FILE fp;
    input(&fp);
    output(&fp);
    return 0;
}