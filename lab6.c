#include<stdio.h>
void input(int *m,int *n)
{
   printf("Enter the number of students:");
   scanf("%d",m);
   printf("Enter the number of courses:");
   scanf("%d",n);
}
void input_array_elements(int m,int n,int a[m][n])
{
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            printf("Enter the marks of student%d in course%d:",i+1,j+1);
            scanf("%d",&a[i][j]);
        }
        printf("\n");
    }
}
void display(int m,int n,int a[m][n])
{
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
        }
}
void compute(int m,int n,int a[m][n],int hn[n])
{
    for(int j=0;j<n;j++)
    {
        hn[j]=a[0][j];
        for(int i=1;i<m;i++)
        {
          if(a[i][j]>hn[j])
          {
              hn[j]=a[i][j];
          }  
        }
    }
}
void output(int n,int hn[n])
{
    for(int j=0;j<n;j++)
    {
    printf("The highest marks scored in course%d is %d\n",j+1,hn[j]);
}
}
int main()
{
    int m,n;
    input(&m,&n);
    int a[m][n];
    input_array_elements(m,n,a);
    display(m,n,a);
    int hn[n];
    compute(m,n,a,hn);
    output(n,hn);
    return 0;
}