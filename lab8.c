#include<stdio.h>
struct employee
{
	int Eid;
	char Ename[25];
	char DOJ[10];
	float salary;
};
typedef struct employee Employee;
Employee input()
{
	Employee emp;
	printf("Enter the Employee ID,Employee's name,Employee's date of joining and salary of the employee:\n");
	scanf("%d%s%s%f",&emp.Eid,emp.Ename,emp.DOJ,&emp.salary);
	printf("\n");
	return emp;
}
void output(Employee emp)
{
	printf("Employee details:\nEmployee ID:%d\nEmployee name:%s\nDate of joining of the employee:%s\nSalary of the employee:%f\n",emp.Eid,emp.Ename,emp.DOJ,emp.salary);
}
int main()
{
	Employee emp;
	emp=input();
	output(emp);
	return 0;
}
