#include<stdio.h>
#include<math.h>
float input()
{
	float radius;
	printf("Enter the radius of the circle\n");
	scanf("%f",&radius);
	return radius;
}
float compute(float radius)
{
	float area_of_circle;
	const pi=3.14;
	area_of_circle=pi*radius*radius;
	return area_of_circle;
}
void output(float radius,float area_of_circle)
{
	printf("The area of circle with radius %f is %f\n",radius,area_of_circle);
}
int main()
{
	float radius,area_of_circle;
	radius=input();
	area_of_circle=compute(radius);
	output(radius,area_of_circle);
	return 0;
}

