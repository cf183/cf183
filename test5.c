#include<stdio.h>
int input()
{
	int n;
	printf("Enter the integer whose multiples are to be printed\n");
	scanf("%d",&n);
	return n;
}
void output(int prod)
{
	printf("%d\n",prod);
}
void compute(int n)
{
	for(int i=1;i<=100;i++)
	{
		int prod=n*i;
		if(prod<=100)
		{
			output(prod);
		}
	}
}

int main()
{
	int n,prod;
	n=input();
	compute(n);
	return 0;
}

