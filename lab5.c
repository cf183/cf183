#include<stdio.h>
int input()
{
	int n;
	printf("Enter the number of elements:\n");
	scanf("%d",&n);
	return n;
}
void input1(int n,int a[n])
{	
	int i;
	printf("Enter the elements of the array:\n");
	for(i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
}
void output1(int n,int a[n])
{
	printf("Array elements before interchanging:\n");
	for(int i=0;i<n;i++)
	{
		printf("%d",a[i]);
	}
	printf("\n");
}
int to_find_large_pos(int n,int a[n])
{
	int i,large,large_pos;
	large=a[0];
	large_pos=0;
	for(i=1;i<n;i++)
	{

		if(a[i]>large)
		{
			large=a[i];
			large_pos=i;
		}
	}
	return large_pos;
}
int to_find_small_pos(int n,int a[n])
{
	int small,small_pos,i;
	small=a[0];
	small_pos=0;
	for(i=1;i<n;i++)
	{
		if(a[i]<small)
		{
			small=a[i];
			small_pos=i;
		}
	}
	return small_pos;
}
void swap(int n,int a[n],int large_pos,int small_pos)
{
	int temp;
	temp=a[large_pos];
	a[large_pos]=a[small_pos];
	a[small_pos]=temp;
}
void output2(int n,int a[n])
{
	printf("Array elements after interchanging:\n");
	for(int i=0;i<n;i++)
	{
		printf("%d",a[i]);
	}
	printf("\n");
}
int main()
{
	int n;
	n=input();
    int a[n];
	input1(n,a);
	output1(n,a);
	int large_pos,small_pos;
	large_pos=to_find_large_pos(n,a);
	small_pos=to_find_small_pos(n,a);
	swap(n,a,large_pos,small_pos);
	output2(n,a);
	return 0;
}



