#include<stdio.h>
void input(int *m,int *n)
{
	printf("Enter the number of rows in the matrix:\n");
	scanf("%d",m);
	printf("Enter the number of columns in the matrix:\n");
	scanf("%d",n);
}
void input_matrix(int m,int n, int a[m][n])
{
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
		{
			printf("Enter the %d element of row%d:",j+1,i+1);
			scanf("%d",&a[i][j]);
		}
		printf("\n");
	}
}
void display_matrix(int m,int n,int a[m][n])
{
	int i,j;
	printf("The matrix is:\n");
	for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			printf("%d\t",a[i][j]);
		}
		printf("\n");
	}
}
void transpose(int m,int n,int a[m][n],int t[n][m])
{
	int i,j;
	for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			t[j][i]=a[i][j];
		}
	}
}
void output(int m,int n,int t[n][m])
{
	printf("The transpose of the matrix is:\n");
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
		{
			printf("%d\t",t[i][j]);
		}
		printf("\n");
	}
}

int main()
{
	int m,n;
	input(&m,&n);
	int a[m][n];
	input_matrix(m,n,a);
	display_matrix(m,n,a);
	int t[n][m];
	transpose(m,n,a,t);
	output(m,n,t);
	return 0;
}