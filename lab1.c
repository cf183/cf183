#include<stdio.h>
#include<math.h>
void input(float *x1,float *y1,float *x2,float *y2)
{
	printf("Enter the abscissa and ordinate of the first point:\n");
	scanf("%f%f",x1,y1);
	printf("Enter the abscissa and ordinate of the second point:\n");
	scanf("%f%f",x2,y2);
}
float compute(float x1,float y1,float x2,float y2)
{
	float distance;
	distance=sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
	return distance;
}
void output(float x1,float y1,float x2,float y2,float distance)
{
	printf("The distance between the points (%f,%f) and (%f,%f) is %f\n",x1,y1,x2,y2,distance);
}
int main()
{
	float x1,y1,x2,y2;
	float distance;
	input(&x1,&y1,&x2,&y2);
	distance=compute(x1,y1,x2,y2);
	output(x1,y1,x2,y2,distance);
	return 0;
}
