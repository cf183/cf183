#include<stdio.h>
int input()
{
	int n;
	printf("Enter an integer\n");
	scanf("%d",&n);
	return n;
}
int compute(int n)
{
	int rem,sum=0;
	while(n!=0)
	{
		rem=n%10;
		sum+=rem;
		n=n/10;
	}
	return sum;
}
void output(int n,int sum)
{
	printf("The sum of the digits of the number %d is:%d\n",n,sum);
}
int main()
{
	int n,sum;
	n=input();
	sum=compute(n);
	output(n,sum);
	return 0;
}

