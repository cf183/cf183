#include<stdio.h>
int input()
{
	int n;
	printf("Enter an integer\n");
	scanf("%d",&n);
	return n;
}
int compute(int n)
{	
	int rem,rev=0;
	while(n!=0)
	{
		rem=n%10;
		rev=(rev*10)+rem;
		n=n/10;
	}
	return rev;
}
void output(int n,int rev)
{
	printf("The reverse of the number %d is %d",n,rev);
}
int main()
{
	int n,rev;
	n=input();
	rev=compute(n);
	output(n,rev);
	return 0;
}

