#include<stdio.h>
void before_insertion(int prime[10],int n)
{
	printf("Array before inserting:\n");
	for(int i=0;i<n;i++)
	{
		printf("%d,",prime[i]);
	}
}
void modify(int n,int pos,int prime[10],int num)
{
	for(int i=n-1;i>=pos-1;i--)
	{
		prime[i+1]=prime[i];
	}
	prime[pos-1]=num;
	n++;
	printf("\n");
}
void after_insertion(int prime[10],int n)
{
    n=n+1;
	printf("Array after inserting:\n");
	for(int i=0;i<n;i++)
	{
		printf("%d,",prime[i]);
	}
}
int main()
{
int prime[10]={2,3,5,11,13,17,19,23};
	int n=8;
	int num=7;
	int pos=4;
	before_insertion(prime,n);
	modify(n,pos,prime,num);
	after_insertion(prime,n);
	return 0;
}



