#include<stdio.h>
void input(int *a,int *b)
{
	printf("Enter two numbers:\n");
	scanf("%d%d",a,b);
}
int addition(int *a,int *b)
{
	int sum;
	sum=*a+*b;
	return sum;
}
int subtraction(int *a,int *b)
{
	int difference;
	difference=*a-*b;
	return difference;
}
int multiplication(int *a,int *b)
{
	int product;
	product=*a**b;
	return product;
}
int division1(int *a,int *b)
{
	int quotient;
	quotient=*a / *b;
	return quotient;
}
int division2(int *a,int *b)
{
	int rem;
	rem=*a%*b;
	return rem;
}
void output(int *a,int *b,int sum,int difference,int product,int quotient,int rem)
{
	printf("The sum of the two numbers:%d+%d=%d\n",*a,*b,sum);
	printf("The difference of the two numbers:%d-%d=%d\n",*a,*b,difference);
	printf("The product of the two numbers:%d*%d=%d\n",*a,*b,product);
	printf("The quotient of the two numbers:%d/%d=%d\n",*a,*b,quotient);
	printf("The remainder of the two numbers:%d and %d=%d\n",*a,*b,rem);
}
int main()
{
	int a,b;
	input(&a,&b);
	int sum;
    int difference;
    int product;
    int quotient;
    int rem;
	sum=addition(&a,&b);
	difference=subtraction(&a,&b);
	product=multiplication(&a,&b);
	quotient=division1(&a,&b);
	rem=division2(&a,&b);
	output(&a,&b,sum,difference,product,quotient,rem);
	return 0;
}


