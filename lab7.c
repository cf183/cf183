#include<stdio.h>
void input(char str1[10],char str2[10])
{
	printf("Enter the first string:\n");
	scanf("%s",str1);
	printf("Enter the second string:\n");
	scanf("%s",str2);
}
void concatenate(char str1[10],char str2[10],char str3[20])
{
	int i1=0,i2=0,i3=0;
	while(str1[i1]!='\0')
	{
		str3[i3]=str1[i1];
		i1++;
		i3++;
	}
	while(str2[i2]!='\0')
	{
		str3[i3]=str2[i2];
		i2++;
		i3++;
	}
	str3[i3]='\0';
}
void output(char str3[20])
{
	printf("String 3 is %s\n",str3);
}
int main()
{
	char str1[10];
	char str2[10];
	input(str1,str2);
	char str3[20];
	concatenate(str1,str2,str3);
	output(str3);
	return 0;
}
