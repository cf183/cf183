#include<stdio.h>
int input()
{
	int n;
	printf("Enter the number of elements in the array:\n");
	scanf("%d",&n);
	return n;
}
void input_array_elements(int n,int a[n])
{
	printf("Enter the elements of the array:\n");
	for(int i=0;i<n;i++)
	{
		scanf("%d",&a[i]);
	}
}
float compute(int n,int a[n])
{
	int sum=0;
	float average;
	for(int i=0;i<n;i++)
	{
		sum+=a[i];
	}
	average=sum/n;
	return average;
}
void output(int n,int a[n],float average)
{
	int i;
	printf("The average of the elements (");
	for(i=0;i<n-1;i++)
	{
		printf("%d+",a[i]);
	}
	printf("%d)/%d = %f\n",a[i],n,average);
}
int main()
{
	int n;
	int a[n];
	float average;
	n=input();
	input_array_elements(n,a);
	average=compute(n,a);
	output(n,a,average);
	return 0;
}







